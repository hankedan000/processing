class Tree implements Comparable<Tree>
{
  PVector s;
  final float HEIGHT_SCALE = 20.0/400.0;
  float varHeightScale;
  
  boolean tagged = false;
  String tagStr;
  color tagColor;
  
  public Tree()
  {
    s = new PVector();
    varHeightScale = random(5.0/400.0);
  }
  
  public void spawn()
  {
    s.x = random(width());
    s.y = random(height()) + height() / 2;
    tagged = false;
  }
  
  public void respawn()
  {
    s.x = random(width());
    s.y = height();
    tagged = false;
  }
  
  public void sim()
  {
    s.y -= speed;
    
    if (s.y < (getTreeHeight() * -1))
    {
      respawn();
    }
  }
  
  public void tag(String str, color c)
  {
    tagged = true;
    tagStr = str;
    tagColor = c;
    SCORE1.play();
  }
  
  public void draw()
  {
    long treeHeight = getTreeHeight();
    long treeWidth = getTreeWidth();
    
    // shadow
    pushMatrix();
    fill(50,50); noStroke();
    translate(s.x+treeWidth*2,s.y);
    shearX(PI/-4.0);
    shearY(PI/25);
    triangle(
      0,treeHeight,
      treeWidth/2,0,
      treeWidth,treeHeight);
    popMatrix();
      
    // trunk
    fill(#795725); noStroke();
    long trunkWidth = getTrunkWidth();
    rect(
      s.x + treeWidth / 2 - trunkWidth / 2,
      s.y + treeHeight,
      trunkWidth,
      trunkWidth);
    
    // tree
    fill(#389332); noStroke();
    triangle(
      s.x,s.y+treeHeight,
      s.x+treeWidth/2,s.y,
      s.x+treeWidth,s.y+treeHeight);
      
    // draw the tag
    if (tagged)
    {
      fill(tagColor);noStroke();
      textFont(GAME_FONT,(int)(src.width()*40/600));
      textAlign(LEFT);
      text(tagStr,s.x,s.y);
    }
      
    if (DRAW_HITBOXES)
    {
      getHitBox().draw();
    }  
  }
  
  public int compareTo(Tree t)
  {
    int r = 0;
    
    if (s.y < t.s.y)
    {
      r = -1;
    }
    else if (s.y > t.s.y)
    {
      r = 1;
    }
    
    return r;
  }
  
  private long getTreeHeight()
  {
    return (long)(height() * (HEIGHT_SCALE + varHeightScale));
  }
  
  private long getTreeWidth()
  {
    return getTreeHeight() * 3 / 4;
  }
  
  private long getTrunkWidth()
  {
    return getTreeWidth() / 3;
  }
  
  public HitBox getHitBox()
  {
    long treeHeight = getTreeHeight();
    long treeWidth = getTreeWidth();
    
    return new HitBox(
      new PVector(s.x+treeWidth/2,s.y+treeHeight),
      getTrunkWidth()/2);
  }
}
