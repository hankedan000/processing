class Player
{
  class ShredParticle
  {
    PVector s;
    PVector v;
    // time to live
    private int ttl;
    private int MAX_TTL = 50;
    final float SIZE_SCALE = 3.0/300.0; 
    
    public ShredParticle(PVector spawn)
    {
      s = new PVector(spawn.x,spawn.y);
      v = new PVector(random(0.4)-.2,random(-0.1)-speed);
      ttl = MAX_TTL;
    }
    
    public boolean isDead()
    {
      return ttl <= 0;
    }
    
    public void sim()
    {
      s = s.add(v);
      ttl--;
    }
    
    public void draw()
    {
      int size = (int)(width() * SIZE_SCALE);
      fill(150,map(ttl,0,MAX_TTL,0,255));noStroke();
      ellipse(s.x,s.y,size,size);
    }
  }
  
  PVector s;
  float v;
  float a;
  float startCarveV;
  boolean inShred;
  private boolean visible;
  ArrayList<PVector> tail;
  ArrayList<ShredParticle> shred;
  int MAX_TAIL = 100;
  final float SIZE_SCALE = 7.0/300.0;
  final float TAIL_SCALE = 2.0/300.0;
  final float ACC_SCALE = 0.03/300.0;
  final float MAX_VEL_SCALE = 1.0/300.0;
  long distance;
  long points;
  color bodyColor;
  
  public Player(color c)
  {
    s = new PVector();
    tail = new ArrayList();
    shred = new ArrayList();
    visible = false;
    bodyColor = c;
  }
  
  public void respawn()
  {
    tail.clear();
    s.x = width() / 2;
    s.y = height() / 3;
    a = 0;
    v = 0;
    startCarveV = 0;
    inShred = false;
    shred.clear();
    distance = 0;
    points = 0;
  }
  
  public void setColor(color c)
  {
    bodyColor = c;
  }
  
  public void setVisible(boolean visible)
  {
    this.visible = visible;
  }
  
  public boolean isVisible()
  {
    return visible;
  }
  
  public int getSize()
  {
    return (int)(width() * SIZE_SCALE);
  }

  public void addPoints(long p)
  {
    points += p;
  }
  
  public long getScore()
  {
    return points + distance;
  }
  
  public void carve()
  {
    if (abs(v) > (maxVelocity()/2))
    {
      CARVE_LONG.play();
    }
    else
    {
      CARVE_SHORT.play();
    }
    if (a < 0.001 && a > -0.001)
    {
      // if standing still, kick off in the right direction
      a = width * ACC_SCALE;
    }
    else
    {
      startCarveV = v;
      inShred = true;
      a *= -1;
    }
  }
  
  private void pushTail()
  {
    tail.add(new PVector(s.x,s.y));
    if (tail.size() > MAX_TAIL)
    {
      tail.remove(0);
    }
  }
  
  private float maxVelocity()
  {
    return width() * MAX_VEL_SCALE;
  }
  
  public int sign(float num)
  {
    if (num >= 0)
    {
      return 1;
    }
    else
    {
      return -1;
    }
  }
  
  public void sim()
  {
    float maxVel = maxVelocity();
    distance += speed;
    v += a;
    if (abs(v) > maxVel)
    {
      // maintain sign, but cap at MAX_VELOCITY
      v = (abs(v)/v) * maxVel;
    }
    s = s.add(v,0);
    
    if (inShred)
    {
      shred.add(new ShredParticle(s));
      if (sign(startCarveV) != sign(v))
      {
        inShred = false;
      }
    }
    
    for (int ss=0; ss<shred.size(); ss++)
    {
      ShredParticle sp = shred.get(ss);
      sp.sim();
      if (sp.isDead())
      {
        shred.remove(ss);
      }
    }
    
    if (abs(v) > 0)
    {
      pushTail();
      for (int tt=0; tt<tail.size(); tt++)
      {
        tail.get(tt).y -= speed;
      }
    }
  }
  
  public void draw()
  {
    if (!visible)
    {
      return;
    }
    
    int playerSize = getSize();
    int tailSize = (int)(width() * TAIL_SCALE);
    // 0 -> furthest, N -> closest
    if (tail.size() > 0)
    {
      long alphaStep = 255 / tail.size();
      long alpha = 0;
      for (int tt=0; tt<tail.size(); tt++)
      {
        fill(200,alpha); noStroke();
        ellipse(tail.get(tt).x,tail.get(tt).y,tailSize,tailSize);
        alpha += alphaStep;
      }
    }
    
    for (int ss=0; ss<shred.size(); ss++)
    {
      shred.get(ss).draw();
    }
    
    // body
    fill(bodyColor); noStroke();
    ellipse(s.x,s.y,playerSize,playerSize);
    
    if (DRAW_HITBOXES)
    {
      getHitBox().draw();
    }
  }
  
  public HitBox getHitBox()
  {
    int playerSize = (int)(width() * SIZE_SCALE);
    return new HitBox(new PVector(s.x,s.y),playerSize/2);
  }
}
