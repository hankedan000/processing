ScreenRotationController src = new ScreenRotationController();

class ScreenRotationController
{
  private boolean needsResize;
  int rotation;
  int baseRotation;
  int w,h;
  
  ScreenRotationController()
  {
  }
  
  boolean isFullScreen()
  {
    return sketchFullScreen();
  }
  
  void init(int screenWidth, int screenHeight)
  {
    needsResize = true;
    rotation = 0;
    baseRotation = 0;
    w = screenWidth;
    h = screenHeight;
    //frame.setResizable(true);
    needsResize = true;
  }
  
  void setBaseRotation(int n)
  {
    baseRotation = n;
    baseRotation %= 4;
    rotation = 0;
    rotateCCW(n);
  }
  
  void resetRotationToBase()
  {
    rotation = baseRotation;
  }
  
  void rotateCCW(int n)
  {
    rotation += n;
    rotation %= 4;
    needsResize = true;
  }
  
  void applyTransform()
  {
    if (needsResize)
    {
      resizeScreen();
      needsResize = false;
    }
    
    switch (rotation)
    {
      case 0:
        /**
         *  o
         * /|\
         * / \
         */
        // do nothing
        break;
      case 1:
        translate(0,width());
        rotate(PI/-2);
        break;
      case 2:
        translate(width(),height());
        rotate(PI);
        break;
      case 3:
        translate(height(),0);
        rotate(PI/2);
        break;
      default:
        println("rotation " + rotation + " not supported!");
        break;
    }
  }
  
  private void resizeScreen()
  {
    if (isFullScreen())
    {
      // let processing handle screen size
      return;
    }
    
    switch (rotation)
    {
      case 0:
      case 2:
        surface.setSize(w,h);
        break;
      case 1:
      case 3:
        surface.setSize(h,w);
        break;
      default:
        println("resizeScreen not supported for rotation " + rotation + "!");
        break;
    }
  }
  
  int width()
  {
    if (isFullScreen())
    {
      return rotation % 2 == 0 ? displayWidth : displayHeight;
    }
    else
    {
      return w;
    }
  }

  int height()
  {
    if (isFullScreen())
    {
      return rotation % 2 == 0 ? displayHeight : displayWidth;
    }
    else
    {
      return h;
    }
  }
}

int width()
{
  return src.width();
}

int height()
{
  return src.height();
}
