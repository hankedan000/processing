class HitBox
{
  class ProxInfo
  {
    boolean hit;
    PVector vec = new PVector();
  }
  
  PVector c;
  float r;
  
  public HitBox(PVector c, float r)
  {
    this.c = c;
    this.r = r;
  }
  
  public ProxInfo getProxInfo(HitBox other)
  {
    ProxInfo pi = new ProxInfo();
    pi.vec.set(c);
    pi.vec.sub(other.c);
    

    pi.hit = pi.vec.mag() < (r + other.r);
    return pi;
  }
  
  public void draw()
  {
    fill(0,0,0,50);stroke(0);
    ellipse(c.x,c.y,r*2,r*2);
  }
}
