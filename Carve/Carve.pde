import java.util.Arrays;
import processing.sound.*;

enum GameState
{
  eRespawn,eMenu,eStaged,ePlay,eConfigEnter,eConfigure,eConfigComplete
}
GameState gs = GameState.eRespawn;

// configuration sub state
enum ConfigState
{
  eP1Start,eP2Start,eP1Carve,eP2Carve,eRotation,eComplete
}
ConfigState cfgState = ConfigState.eP1Start;
// how long to pres & hold before entering config menu
long PRESS_HOLD_DURATION_MS = 3000;
long pressHoldCountDown = PRESS_HOLD_DURATION_MS;

// BEGIN DEBUG
boolean DRAW_HITBOXES = false;
// END DEBUG

float GAME_VERSION = 1.0;
String GAME_DATA_FILE = "carve.json";
SoundFile SCORE1;
SoundFile BACKGROUND;
SoundFile CARVE_LONG;
SoundFile CARVE_SHORT;
SoundFile GRUNT;
SoundFile WILHELM;

int numPlayers = 0;
int currPlayer = 1;
int p1Lives = 0;
int p2Lives = 0;
color P1_COLOR = #9696FF;
color P2_COLOR = #FFAA3C;

PFont GAME_FONT;
float DOWNHILL_SPEED_SCALE = 1.5/400.0;
float speed = 0;
Tree[] trees = new Tree[40];
Player p = new Player(P1_COLOR);
long prevDrawStart_ms;
boolean rotate = false;
// multipliers against tree widths
float[] proxBins = {1.5,1.0,0.5};
long[] scoreBins = {25,50,100};
color[] colorBins = {#F79B0F,#0FAFF7,#9D0FF7};

// key flags
class KeyCodes
{
  int p1start = 0;
  int p2start = 0;
  int p1carve = 0;
  int p2carve = 0;
}
KeyCodes gameKeys = new KeyCodes();
boolean pressedKey = false;
boolean releasedKey = false;
int pressedKeyCode = 0;
boolean p1CarvePressed = false;
boolean p2CarvePressed = false;

// time to live (used for off course)
int MAX_TTL_MS = 3000;
int ttl_ms = MAX_TTL_MS;

// game's historical data (saved to disk each death)
JSONObject gameData;

void setup()
{
  GAME_FONT = loadFont("BalooPaaji-Regular-48.vlw");
  SCORE1 = new SoundFile(this, "score1.wav");
  SCORE1.amp(0.75);
  BACKGROUND = new SoundFile(this, "background.mp3");
  CARVE_LONG = new SoundFile(this, "carve_long.wav");
  CARVE_LONG.amp(0.75);
  CARVE_SHORT = new SoundFile(this, "carve_short.wav");
  CARVE_SHORT.amp(0.75);
  GRUNT = new SoundFile(this, "grunt.wav");
  GRUNT.amp(0.75);
  WILHELM = new SoundFile(this, "wilhelm.wav");
  WILHELM.amp(0.75);
  BACKGROUND.loop();
  fullScreen(1);
  noCursor();
  surface.setAlwaysOnTop(true);
  src.init(300,400);
  loadGameData();
  src.setBaseRotation(gameData.getInt("rotation"));
  
  for (int tt=0; tt<trees.length; tt++)
  {
    trees[tt] = new Tree();
  }
}

void loadGameData()
{
  try
  {
    gameData = loadJSONObject(GAME_DATA_FILE);
    gameKeys.p1start = gameData.getInt("p1start");
    gameKeys.p2start = gameData.getInt("p2start");
    gameKeys.p1carve = gameData.getInt("p1carve");
    gameKeys.p2carve = gameData.getInt("p2carve");
  }
  catch (Exception ex)
  {
    // file doesn't exist yet!
    gameData = new JSONObject();
    gameData.setFloat("version",GAME_VERSION);
    gameData.setLong("highscore",0);
    gameData.setLong("playcount",0);
    gameData.setInt("p1start",0);
    gameData.setInt("p2start",0);
    gameData.setInt("p1carve",0);
    gameData.setInt("p2carve",0);
    gameData.setInt("rotation",0);
    
    // reconfigure game
    gs = GameState.eConfigEnter;
  }
}

void saveGameData()
{
  gameData.setInt("p1start",gameKeys.p1start);
  gameData.setInt("p2start",gameKeys.p2start);
  gameData.setInt("p1carve",gameKeys.p1carve);
  gameData.setInt("p2carve",gameKeys.p2carve);
  saveJSONObject(gameData,GAME_DATA_FILE);
}

void mousePressed()
{
  pressedKey = true;
}

void keyPressed()
{
  pressedKey = true;
  releasedKey = false;
  pressedKeyCode = keyCode;
  if (keyCode == gameKeys.p1start)
  {
    pressedKey = true;
  }
  else if (keyCode == gameKeys.p2start)
  {
    pressedKey = true;
  }
  else if (keyCode == gameKeys.p1carve)
  {
    p1CarvePressed = true;
  }
  else if (keyCode == gameKeys.p2carve)
  {
    p2CarvePressed = true;
  }
}

void keyReleased()
{
  pressHoldCountDown = PRESS_HOLD_DURATION_MS;
  releasedKey = true;
}

void respawn()
{
  for (int tt=0; tt<trees.length; tt++)
  {
    trees[tt].spawn();
  }
  // Sort so draw order is from highest to lowest
  Arrays.sort(trees);
  p.respawn();
}
 
float scoreBoxH()
{
  return speed * 1.5;
}

long getHighscore()
{
  return gameData.getLong("highscore");
}

void updateHighscore(long score)
{
  if (score > getHighscore())
  {
    gameData.setLong("highscore",score);
  }
}

void incrementPlaycount()
{
  long playcount = gameData.getLong("playcount");
  playcount++;
  gameData.setLong("playcount",playcount);
}

void handleConfigMenuStates()
{
  if (pressedKey)
  {
    pressedKey = false;
    switch(cfgState)
    {
      case eP1Start:
        gameKeys.p1start = pressedKeyCode;
        cfgState = ConfigState.eP2Start;
        break;
      case eP2Start:
        gameKeys.p2start = pressedKeyCode;
        cfgState = ConfigState.eP1Carve;
        break;
      case eP1Carve:
        gameKeys.p1carve = pressedKeyCode;
        cfgState = ConfigState.eP2Carve;
        break;
      case eP2Carve:
        gameKeys.p2carve = pressedKeyCode;
        cfgState = ConfigState.eRotation;
        break;
      case eRotation:
        if (pressedKeyCode == gameKeys.p1carve)
        {
          int rotation = gameData.getInt("rotation");
          rotation++;
          gameData.setInt("rotation",rotation);
          src.setBaseRotation(rotation);
        }
        else if (pressedKeyCode == gameKeys.p1start)
        {
          cfgState = ConfigState.eComplete;
        }
        break;
      case eComplete:
        break;
    }
  }
}

void drawConfigMenu()
{
  String text = "***INVALID STATE***";
  switch(cfgState)
  {
    case eP1Start:
      text = "Press P1 Start Button";
      break;
    case eP2Start:
      text = "Press P2 Start Button";
      break;
    case eP1Carve:
      text = "Press P1 Carve Button";
      break;
    case eP2Carve:
      text = "Press P2 Carve Button";
      break;
    case eRotation:
      text = "Scren Rotation Config:\n'P1 Carve' to rotate.\n'P1 Start' to save.";
      break;
  }
  
  fill(#989898);    
  textAlign(CENTER);
  textFont(GAME_FONT,(int)(src.width()*20.0/300));
  text(text,src.width()/2,src.height()/2);
}

void draw()
{
  // compute delta time in milliseconds between draws
  float dt_ms = 1000.0/frameRate;
  //println("fps = ",frameRate);
  
  background(255);
  boolean offCourse = false;
  boolean showMenu = false;
  boolean showStaging = false;
  
  src.applyTransform();
  
  switch (gs)
  {
    case eConfigEnter:
      drawConfigMenu();
      if (releasedKey)
      {
        releasedKey = false;
        pressedKey = false;
        cfgState = ConfigState.eP1Start;
        gs = GameState.eConfigure;
      }
      break;
    case eConfigure:
      handleConfigMenuStates();
      drawConfigMenu();
      
      if (cfgState == ConfigState.eComplete)
      {
        gs = GameState.eConfigComplete;
      }
      break;
    case eConfigComplete:
      saveGameData();
      numPlayers = 0;
      p1Lives = 0;
      p2Lives = 0;
      gs = GameState.eRespawn;
      break;
    case eRespawn:
      // update game data and save
      updateHighscore(p.getScore());
      incrementPlaycount();
      saveGameData();
    
      pressHoldCountDown = PRESS_HOLD_DURATION_MS;
      speed = 0;
      ttl_ms = MAX_TTL_MS;
      pressedKey = false;
      respawn();
      gs = GameState.eStaged;
      if (p1Lives == 0 && p2Lives == 0)
      {
        gs = GameState.eMenu;
      }
      else
      {
        src.rotateCCW(2);
        currPlayer++;
      }
      break;
    case eMenu:
      src.resetRotationToBase();
      p.setVisible(false);
      showMenu = true;
      if (pressedKey)
      {
        if (pressedKeyCode == gameKeys.p1start ||
            pressedKeyCode == gameKeys.p2start)
        {
          numPlayers = 1;
          if (pressedKeyCode == gameKeys.p2start)
          {
            numPlayers = 2;
          }
        }
        
        pressHoldCountDown -= dt_ms;
        println(pressHoldCountDown);
        if (pressHoldCountDown < 0)
        {
          gs = GameState.eConfigEnter;
        }
        
        p1Lives = 1;
        p2Lives = 0;
        if (numPlayers > 1)
        {
          p2Lives = 1;
        }
        currPlayer = 1;
        if (releasedKey && numPlayers > 0)
        {
          gs = GameState.eStaged;
        }
      }
      if (releasedKey)
      {
        pressedKey = false;
      }
      break;
    case eStaged:
      p.setColor(currPlayer == 1 ? P1_COLOR : P2_COLOR);
      p.setVisible(true);
      showStaging = true;
      
      if (pressedKey)
      {
        if (currPlayer == 1 && pressedKeyCode == gameKeys.p1carve)
        {
          p1Lives--;
          gs = GameState.ePlay;
          p.carve();
        }
        else if (currPlayer == 2 && pressedKeyCode == gameKeys.p2carve)
        {
          p2Lives--;
          gs = GameState.ePlay;
          p.carve();
        }
        pressedKey = false;
      }
      break;
    case ePlay:
      speed = DOWNHILL_SPEED_SCALE * height();
      if (pressedKey)
      {
        if (currPlayer == 1 && pressedKeyCode == gameKeys.p1carve)
        {
          p.carve();
        }
        else if (currPlayer == 2 && pressedKeyCode == gameKeys.p2carve)
        {
          p.carve();
        }
        pressedKey = false;
      }
      
      // handle tree proximity scoring
      HitBox pHB = p.getHitBox();
      float[] proxDist = new float[proxBins.length];
      for (int tt=0; tt<trees.length; tt++)
      {
        HitBox tHB = trees[tt].getHitBox();
        HitBox.ProxInfo pi = tHB.getProxInfo(pHB);
        long tw = trees[tt].getTreeWidth();
        
        // really only needs to be done once?!?!
        if (tt==0)
        {
          for (int bb=0; bb<proxBins.length; bb++)
          {
            proxDist[bb] = proxBins[bb] * tw;
          }
        }
        
        if (pi.vec.y < 0 && pi.vec.y > -scoreBoxH())
        {
          // tree is inside proximity window
          float prox = pi.vec.mag();
          int bin = -1;
          for (int bb=0; bb<proxDist.length; bb++)
          {
            if (prox < proxDist[bb])
            {
              bin++;
            }
            else
            {
              // not even close!!!
              break;
            }
          }
          
          if (bin >= 0)
          {
            p.addPoints(scoreBins[bin]);
            trees[tt].tag(Long.toString(scoreBins[bin]),colorBins[bin]);
          }
        }
        
        if (pi.hit)
        {
          GRUNT.play();
          gs = GameState.eRespawn;
          break;
        }
      }
      
      // see if player is off screen
      if (p.s.x < 0 || p.s.x > (src.width() - p.getSize()))
      {
        offCourse = true;
      }
      break;
  }
  
  // sim and draw player
  p.sim();
  p.draw();
  
  // sim and draw trees
  for (int tt=0; tt<trees.length; tt++)
  {
    trees[tt].sim();
    trees[tt].draw();
  }
  
  // draw score text
  float scoreBaseline = src.width()*20.0/300.0;
  float buffer = src.width()*10.0/300.0;
  fill(0,0,0);
  textFont(GAME_FONT,(int)(src.width()*14.0/300));
  textAlign(LEFT);
  text(Long.toString(p.getScore()),buffer,scoreBaseline);
  
  fill(#479D48);
  textAlign(RIGHT);
  text(String.format("HIGHSCORE: %d",getHighscore()),src.width()-buffer,scoreBaseline);
  
  // draw menu
  float menuBaseline = src.height()*1.0/4.0;
  if (showMenu)
  {
    String menuString = "1 or 2 Players?";
    if (numPlayers > 0)
    {
      menuString += "\nOr continue?";
    }
    
    fill(#989898);
    textAlign(CENTER);
    textFont(GAME_FONT,(int)(src.width()*20.0/300));
    text(menuString,src.width()/2,menuBaseline);
    
    fill(#D4D6D6);
    textFont(GAME_FONT,(int)(src.width()*10.0/300));
    text("press & hold any key to reconfigure",src.width()/2,menuBaseline+src.height()*0.1);
  }
  
  // draw staging text
  if (showStaging)
  {
    String stageString = String.format("Player %d\nPress button to carve!",currPlayer);
    fill(#989898);    
    textAlign(CENTER);
    textFont(GAME_FONT,(int)(src.width()*20.0/300));
    text(stageString,src.width()/2,menuBaseline);
  }
  
  // draw OFF COURSE warning
  if (offCourse)
  {
    // decrement time to live when off course
    ttl_ms -= dt_ms;
    if (ttl_ms < 0)
    {
      WILHELM.play();
      gs = GameState.eRespawn;
    }
    
    // blood fill
    float alpha = map(ttl_ms,MAX_TTL_MS,0,0,255);
    fill(255,0,0,alpha);
    rect(0,0,src.width(),src.height());
  }
}
