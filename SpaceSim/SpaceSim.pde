GravityField F;
Planet me;
PVector startClick,endClick;

void setup(){
  size(1500,600);
  randomSeed(second());
  
  F = new GravityField(0.0000005);
  Planet sun = new Planet(250,height/2,100000);
  me = new Planet(0,0,5000);
  sun.setFixed(true);
  F.addPlanet(sun);
  sun = new Planet(400,height/2,100000);
  sun.setFixed(true);
  F.addPlanet(sun);
}// setup()

void draw(){
  background(0);
  if(startClick!=null){
    stroke(250,0,50);
    line(startClick.x,startClick.y,mouseX,mouseY);
    me.drawTrajectory(F,0.1*(startClick.x-mouseX),0.1*(startClick.y-mouseY));
  } else {
    me.setPosition(mouseX,mouseY);
  }// if
  
  me.draw();
  F.draw();
}// draw()

void mousePressed(){
  startClick = new PVector(mouseX,mouseY);
}

void mouseReleased(){
  endClick = new PVector(mouseX,mouseY);
  me.setVelocity(
    0.1*(startClick.x-endClick.x),
    0.1*(startClick.y-endClick.y)
  );
  F.addPlanet(me);
  
  me = new Planet(mouseX,mouseY,5000);
  startClick = null;
  endClick = null;
}

void mouseWheel(MouseEvent e){
  me.setMass(me.getMass()+(int)(-1*e.getCount()*me.getMass()*0.1));
  println(me.getMass());
  me.draw();
}