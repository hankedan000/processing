class GravityField{
  ArrayList<Planet> P;
  float g;
  
  GravityField(float magnitude){
    P = new ArrayList();
    g = magnitude;
  }
  
  public int getPlanetCount(){
    return P.size();
  }
  
  public void addPlanet(Planet planet){
    P.add(planet);
  }
  
  public PVector computeGravity(Planet p){
    Planet p2;
    PVector result = new PVector(0,0);;
    
    for(int i=0; i<getPlanetCount(); i++){
      p2 = P.get(i);// other planets in field
      if(p!=p2){
        result.add(p.getGravitationalForce(p2));
      }// if
    }// for
    result.mult(g);// scale to gravity field magnitude constant
    
    return result;
  }
  
  public void draw(){
    PVector newAcc;
    
    for(Planet p : P){
      
      // Compute new acceleration
      if(!p.isFixed()){
        newAcc = computeGravity(p);
        p.setAcceleration(newAcc.x,newAcc.y);
      }// if
      
      // Draw
      p.nextPosition();
      p.draw();
    }// for
  }
}