class Planet{
  private PVector s,v,a;
  private double mass;
  private int radius;
  private boolean fixed;
  private color c;
  
  Planet(int x, int y, int m){
    c = color(random(255),random(255),random(255));
    s = new PVector(x,y);
    v = new PVector(0,0);
    a = new PVector(0,0);
    setMass(m);
    fixed = false;
  }// Planet
  
  Planet(Planet p){
    c = p.getFill();
    s = new PVector(p.getPosition().x,p.getPosition().y);
    v = new PVector(p.getVelocity().x,p.getVelocity().y);
    a = new PVector(p.getAcceleration().x,p.getAcceleration().y);
    setMass(p.getMass());
    fixed = p.isFixed();
  }
  
  public void setMass(double m){
    mass = m;
    radius = (int)Math.pow((3.0/4.0)*m,(1.0/3.0));
  }
  
  public double getMass(){
    return mass;
  }
  
  public void setPosition(float x, float y){
    s.set(x,y);
  }
  
  public PVector getPosition(){
    return s;
  }
  
  public void setVelocity(float x, float y){
    v.set(x,y);
  }
  
  public PVector getVelocity(){
    return v;
  }
  
  public void setAcceleration(float x, float y){
    a.set(x,y);
  }
  
  public PVector getAcceleration(){
    return a;
  }
  
  public float getDist(Planet planet){
    return s.dist(planet.getPosition());
  }
  
  public void setFixed(boolean f){
    fixed = f;
  }
  
  public boolean isFixed(){
    return fixed;
  }
  
  public color getFill(){
    return c;
  }
  
  public PVector getGravitationalForce(Planet planet){
    PVector s2 = planet.getPosition();
    // create a vector toward the other planet
    PVector result = new PVector(s2.x-s.x,s2.y-s.y);
    // set magnetude to fuction of gravitational force
    result.setMag((float)(mass*planet.getMass())/getDist(planet));
    
    return result;
  }
  
  public void drawTrajectory(GravityField F, float vx, float vy){
    Planet copy = new Planet(this);
    PShape traj;
    PVector tempVector;
    
    traj = createShape();
    traj.beginShape();
    traj.noFill();
    traj.stroke(255);
    copy.setVelocity(vx,vy);
    for(int i=0; i<50; i++){
      tempVector = F.computeGravity(copy);// get new acceleration
      copy.setAcceleration(tempVector.x,tempVector.y);// set new acceleration
      tempVector = copy.nextPosition();// get new position
      traj.vertex(tempVector.x,tempVector.y);
    }// for
    copy = null;
    traj.endShape();
    
    shape(traj,0,0);
  }
  
  public PVector nextPosition(){
    v.add(a);
    s.add(v);
    
    return s;
  }
  
  public void draw(){
    fill(c);
    stroke(c);
    ellipse(s.x,s.y,radius,radius);
  }
}