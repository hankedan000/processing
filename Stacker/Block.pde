class Block
{
  float x,w,h;
  private color c;
  
  public Block(float x, float w, float h)
  {
    this.x = x;
    this.w = w;
    this.h = h;
    c = 255;
  }
  
  public Block(Block other)
  {
    x = other.x;
    w = other.w;
    h = other.h;
    c = other.c;
  }
  
  public void draw(int level)
  {
    stroke(0);
    strokeWeight(1);
    fill(c);
    rect(x,height-level*h,w,h);
  }
}
