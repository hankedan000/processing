int BLOCK_HEIGHT = 0;
final color BG = #B7F0FF;
final color RECORD = #FF6467;
final color GOAL = #4FD359;
final int MAX_LEVELS = 20;
int record = -1;
int level = 1;
Block cb;
float v = 0;
float dir = 1;
color fills[] = {
  #FF3B3B,// red
  #FFB13B,// orange
  #FFF93B,// yellow
  #47FF3B,// green
  #3B5AFF,// blue
  #D13BFF // purple
};

final int RESET = 0;
final int SPAWN_NEW = 1;
final int MOVING = 2;
final int DEAD = 3;
int currState = RESET;
boolean pressed = false;

ArrayList<Block> blocks = new ArrayList();

void setup()
{
  size(300,400);
  BLOCK_HEIGHT = height/MAX_LEVELS;
  cb = new Block(width/4,width/2,BLOCK_HEIGHT);
  
  resetGame();
}

void resetGame()
{
  level = 1;
  cb.w = width/2;
  cb.x = width/4;
  cb.c = getFill(level);
  v = width/3.0;// 3 seconds to move accros screen
  blocks.clear();
}

void keyPressed()
{
  pressed = true;
}

Block getNextBlock(Block prev, Block curr)
{
  Block next = new Block(curr);
  
  if ((curr.x+curr.w) < prev.x)
  {
    next = null;
  }
  else if (curr.x > (prev.x+prev.w))
  {
    next = null;
  }
  else if (prev.x < curr.x)
  {
    next.w = prev.x+prev.w-curr.x;
  }
  else
  {
    next.w = curr.w-(prev.x-curr.x);
    next.x = prev.x;
  }
  
  return next;
}

color getFill(int currLevel)
{
  return fills[(currLevel-1)%fills.length];
}

/**
 * draws a marker line across the screen at the given level
 *
 * @param[in] c
 * the line stroke color
 *
 * @param[in] l
 * the line height in levels (0 is the bottom of the screen)
 */
void drawLine(color c, int l, String label)
{
  stroke(c);
  strokeWeight(max(2,BLOCK_HEIGHT*0.1));
  int lineY = height-l*BLOCK_HEIGHT;
  line(0,lineY,width,lineY);
  
  fill(c);
  text(label,0,lineY-2);
}

void draw()
{
  background(BG);
  // compute delta time from previous frame
  float dt = 1.0/frameRate;
  int nextState = currState;
  
  switch (currState)
  {
    case RESET:
      resetGame();
      nextState = MOVING;
      break;
    case SPAWN_NEW:
      Block next = cb;
      if (blocks.size() != 0)
      {
        // get previous block
        Block ps = blocks.get(blocks.size()-1);
        next = getNextBlock(ps,cb);
      }
      
      if (next != null)
      {
        level++;
        // add current block to history
        blocks.add(next);
        cb = new Block(next);
        cb.c = getFill(level);
        v += 10;
        nextState = MOVING;
      }
      else
      {
        v = 0;
        nextState = DEAD;
      }
      break;
    case MOVING:
      cb.x += dir*v*dt;
      if ((cb.x + cb.w) > width || cb.x < 0)
      {
        dir *= -1;
      }
      if (pressed)
      {
        nextState = SPAWN_NEW;
      }
      break;
    case DEAD:
      if (pressed)
      {
        record = max(record,level - 1);
        nextState = RESET;
      }
      break;
  }
  
  // modify stateful variables
  pressed = false;
  currState = nextState;
  
  // ------------------------------
  
  // draw current block
  cb.draw(level);
  
  // draw previous blocks
  int i = 1;
  for (Block b : blocks)
  {
    b.draw(i++);
  }
  
  // draw marker lines
  drawLine(GOAL,MAX_LEVELS-2,"GOAL");
  if (record > 0)
  {
    drawLine(RECORD,record,"RECORD");
  }
}
