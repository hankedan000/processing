PImage img;

Player p;
int hitboxCheck=2;
float slide=0.9;
int win=0;
int currLevelIdx=0;
int[][] currLevel;
int d_d=0;
int a_a=0;
int s_s=0;
int w_w=0;
int s_sp=0;
float[] ball_size={10, 10, 10, 10, 10, 10, 10, 5, 10, 10, 10, 10, 10, 10, 10, 10, 10};
int mode=1;
void setup() {
  size(500, 500);
  p=new Player();
  currLevel = level[currLevelIdx];
  p.prep();
  frameRate(60);
}
void draw() {
  background(51);
  p.move();
  p.show();
  if (win==1) {
    nextLevel();
    win=0;
  }
}

void nextLevel()
{
  currLevelIdx++;
  currLevelIdx %= level.length;
  currLevel = level[currLevelIdx];
  p.prep();
}

void keyReleased() {
  if (key==' ') {
    s_s=0;
    p.sd=1;
  }
  if (key=='a') {
    a_a=0;
  }
  if (key=='w') {
    w_w=0;
  }
  if (key=='s') {
    s_sp=0;
  }
  if (key=='d') {
    d_d=0;
  }
  if (key=='A') {
    a_a=0;
  }
  if (key=='W') {
    w_w=0;
  }
  if (key=='S') {
    s_sp=0;
  }
  if (key=='D') {
    d_d=0;
  }
  if (keyCode==LEFT) {
    a_a=0;
  }
  if (keyCode==RIGHT) {
    d_d=0;
  }
  if (keyCode==UP) {
    w_w=0;
  }
  if (keyCode==DOWN) {
    s_sp=0;
  }
}
void keyPressed() {
  if (key==' ') {
    s_s=1;
  }
  if (key=='a') {
    a_a=1;
  }
  if (key=='d') {
    d_d=1;
  }
  if (key=='w') {
    w_w=1;
  }
  if (key=='s') {
    s_sp=1;
  }
  if (key=='A') {
    a_a=1;
  }
  if (key=='D') {
    d_d=1;
  }
  if (key=='W') {
    w_w=1;
  }
  if (key=='S') {
    s_sp=1;
  }
  if (keyCode==LEFT) {
    a_a=1;
  }
  if (keyCode==RIGHT) {
    d_d=1;
  }
  if (keyCode==UP) {
    w_w=1;
  }
  if (keyCode==DOWN) {
    s_sp=1;
  }
}
class Player {
  PVector pos;
  PVector vel=new PVector(0, 0);
  PVector acc=new PVector(0, 0);
  PVector maxvel=new PVector(ball_size[currLevelIdx]/2, ball_size[currLevelIdx]/2);
  int p=0;
  int sd=1;
  //movement speed
  float mm=2;
  //gravity
  float g=1;
  float hg=0;
  float bounce=0.5;
  float bounce_2=1.5;
  void prep() {
    maxvel=new PVector(ball_size[currLevelIdx]/2, ball_size[currLevelIdx]/2);
    for (int y=0; y<currLevel.length; y++) {
      for (int x=0; x<currLevel[y].length; x++) {
        if (currLevel[y][x]==1) {
          pos=new PVector((width/currLevel[y].length)*x+(width/currLevel[y].length)/2, (height/currLevel.length)*y+(height/currLevel.length)/2);
          vel.x=0;
          vel.y=0;
          g=1;
          hg=0;
          return;
        }
      }
    }
  }
  void move() {
    if (keyPressed==false) {
      if (g!=0) {
        float save=vel.y;
        vel.set(vel.x*slide, save);
      } else {
        float save=vel.x;
        vel.set(save, vel.y*slide);
      }
    }
    PVector att=new PVector(pos.x, pos.y);
    int k=0;
    if ((d_d==1)&&(g!=0)) {
      att.add(mm, 0);
      att.sub(pos);
      k=1;
    }
    if ((a_a==1)&&(g!=0)) {
      att.add(-mm, 0);
      att.sub(pos);
      k=1;
    }
    if ((w_w==1)&&(hg!=0)) {
      att.add(0, -mm);
      att.sub(pos);
      k=1;
    }
    if ((s_sp==1)&&(hg!=0)) {
      att.add(0, mm);
      att.sub(pos);
      k=1;
    }
    if ((s_s==1)&&(sd==1)) {
      g*=-1;
      hg*=-1;
      sd=0;
    }
    if (k==0) {
      att.sub(pos);
    }
    if (g!=0) {
      acc.set(att.x, 0);
    } else {
      acc.set(0, att.y);
    }
    if (mode==0) {
      acc.add(hg, g);
    }
    acc.setMag(0.5);
    if (mode==1) {
      acc.add(hg, g);
    }
    vel.add(acc);
    if (vel.x>maxvel.x) {
      vel.x=maxvel.x;
    }
    if (vel.y>maxvel.y) {
      vel.y=maxvel.y;
    }
    if (vel.x<-maxvel.x) {
      vel.x=-maxvel.x;
    }
    if (vel.y<-maxvel.y) {
      vel.y=-maxvel.y;
    }
    vel.div(hitboxCheck);
    for (int lo=0; lo<hitboxCheck; lo++) {
      pos.add(vel);
      hitbox();
    }
    vel.mult(hitboxCheck);
    if (pos.x<0) {
      prep();
    }
    if (pos.x>width) {
      prep();
    }
    if (pos.y<0) {
      prep();
    }
    if (pos.y>height) {
      prep();
    }
    int gs=0;
    for (int i=0; i<currLevel.length; i++) {
      for (int j=0; j<currLevel[i].length; j++) {
        if (currLevel[i][j]==5) {
          gs=1;
        }
      }
    }

    if (gs==0) {
      for (int i=0; i<currLevel.length; i++) {
        for (int j=0; j<currLevel[i].length; j++) {
          if (currLevel[i][j]==6) {
            currLevel[i][j]=3;
          }
        }
      }
    } else {
      for (int i=0; i<currLevel.length; i++) {
        for (int j=0; j<currLevel[i].length; j++) {
          if (currLevel[i][j]==3) {
            currLevel[i][j]=6;
          }
        }
      }
    }
    //-------------------------------
    gs=0;
    for (int i=0; i<currLevel.length; i++) {
      for (int j=0; j<currLevel[i].length; j++) {
        if (currLevel[i][j]==15) {
          gs=1;
        }
      }
    }

    if (gs==0) {
      for (int i=0; i<currLevel.length; i++) {
        for (int j=0; j<currLevel[i].length; j++) {
          if (currLevel[i][j]==14) {
            currLevel[i][j]=-14;
          }
        }
      }
    } else {
      for (int i=0; i<currLevel.length; i++) {
        for (int j=0; j<currLevel[i].length; j++) {
          if (currLevel[i][j]==-14) {
            currLevel[i][j]=14;
          }
        }
      }
    }

    if (frameCount%60==0) {
      for (int i=0; i<currLevel.length; i++) {
        for (int j=0; j<currLevel[i].length; j++) {
          if ((currLevel[i][j]==9)||(currLevel[i][j]==8)) {
            if (currLevel[i][j]==8) {
              currLevel[i][j]=9;
            } else {
              currLevel[i][j]=8;
            }
          }
        }
      }
    }
  }
  void hitbox() {
    for (int i=0; i<currLevel.length; i++) {
      for (int j=0; j<currLevel[i].length; j++) {
        //block
        if ((currLevel[i][j]==2)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          pos.set(pos.x, (height/currLevel.length)*i-(ball_size[currLevelIdx]/2+1));
          vel.y*=-bounce;
        }
        if ((currLevel[i][j]==2)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          pos.set(pos.x, (height/currLevel.length)*(i+1)+(ball_size[currLevelIdx]/2+1));
          vel.y*=-bounce;
        }
        if ((currLevel[i][j]==2)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          pos.set((width/currLevel[i].length)*j-(ball_size[currLevelIdx]/2+1), pos.y);
          vel.x*=-bounce;
        }
        if ((currLevel[i][j]==2)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          pos.set((width/currLevel[i].length)*(j+1)+(ball_size[currLevelIdx]/2+1), pos.y);
          vel.x*=-bounce;
        }
        //change block
        if ((currLevel[i][j]==8)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          pos.set(pos.x, (height/currLevel.length)*i-(ball_size[currLevelIdx]/2+1));
          vel.y*=-bounce;
        }
        if ((currLevel[i][j]==8)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          pos.set(pos.x, (height/currLevel.length)*(i+1)+(ball_size[currLevelIdx]/2+1));
          vel.y*=-bounce;
        }
        if ((currLevel[i][j]==8)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          pos.set((width/currLevel[i].length)*j-(ball_size[currLevelIdx]/2+1), pos.y);
          vel.x*=-bounce;
        }
        if ((currLevel[i][j]==8)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          pos.set((width/currLevel[i].length)*(j+1)+(ball_size[currLevelIdx]/2+1), pos.y);
          vel.x*=-bounce;
        }
        //death
        if ((currLevel[i][j]==4)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          for (int l=0; l<currLevel.length; l++) {
            for (int o=0; o<currLevel[l].length; o++) {
              if (currLevel[l][o]==-1) {
                currLevel[l][o]=5;
              }
              if (currLevel[l][o]==-15) {
                currLevel[l][o]=15;
              }
            }
          }
          g=1;
          hg=0;
          prep();
          vel.set(0, 0);
        }
        if ((currLevel[i][j]==4)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          for (int l=0; l<currLevel.length; l++) {
            for (int o=0; o<currLevel[l].length; o++) {
              if (currLevel[l][o]==-1) {
                currLevel[l][o]=5;
              }
              if (currLevel[l][o]==-15) {
                currLevel[l][o]=15;
              }
            }
          }
          g=1;
          hg=0;
          prep();
          vel.set(0, 0);
        }
        if ((currLevel[i][j]==4)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          for (int l=0; l<currLevel.length; l++) {
            for (int o=0; o<currLevel[l].length; o++) {
              if (currLevel[l][o]==-1) {
                currLevel[l][o]=5;
              }
              if (currLevel[l][o]==-15) {
                currLevel[l][o]=15;
              }
            }
          }
          g=1;
          hg=0;
          prep();
          vel.set(0, 0);
        }
        if ((currLevel[i][j]==4)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          for (int l=0; l<currLevel.length; l++) {
            for (int o=0; o<currLevel[l].length; o++) {
              if (currLevel[l][o]==-1) {
                currLevel[l][o]=5;
              }
              if (currLevel[l][o]==-15) {
                currLevel[l][o]=15;
              }
            }
          }
          g=1;
          hg=0;
          prep();
          vel.set(0, 0);
        }

        //goal

        if ((currLevel[i][j]==3)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          win=1;
          g=1;
          hg=0;
        }
        if ((currLevel[i][j]==3)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          win=1;
          g=1;
          hg=0;
        }
        if ((currLevel[i][j]==3)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          win=1;
          g=1;
          hg=0;
        }
        if ((currLevel[i][j]==3)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          win=1;
          g=1;
          hg=0;
        }

        //key
        if ((currLevel[i][j]==5)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          currLevel[i][j]=-1;
        }
        if ((currLevel[i][j]==5)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          currLevel[i][j]=-1;
        }
        if ((currLevel[i][j]==5)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          currLevel[i][j]=-1;
        }
        if ((currLevel[i][j]==5)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          currLevel[i][j]=-1;
        }

        //bounce pad
        if ((currLevel[i][j]==7)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          vel.y=-maxvel.y;
          vel.y*=bounce_2;
        }
        if ((currLevel[i][j]==7)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          vel.y=maxvel.y;
          vel.y*=bounce_2;
        }
        if ((currLevel[i][j]==7)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          vel.x=-maxvel.x;
          vel.x*=bounce_2;
        }
        if ((currLevel[i][j]==7)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          vel.x=maxvel.x;
          vel.x*=bounce_2;
        }

        //gravity change

        //change up
        if ((currLevel[i][j]==10)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          g=-1;
          hg=0;
        }
        if ((currLevel[i][j]==10)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          g=-1;
          hg=0;
        }
        if ((currLevel[i][j]==10)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          g=-1;
          hg=0;
        }
        if ((currLevel[i][j]==10)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          g=-1;
          hg=0;
        }
        //change right
        if ((currLevel[i][j]==11)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          g=0;
          hg=1;
        }
        if ((currLevel[i][j]==11)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          g=0;
          hg=1;
        }
        if ((currLevel[i][j]==11)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          g=0;
          hg=1;
        }
        if ((currLevel[i][j]==11)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          g=0;
          hg=1;
        }
        //change down
        if ((currLevel[i][j]==12)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          g=1;
          hg=0;
        }
        if ((currLevel[i][j]==12)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          g=1;
          hg=0;
        }
        if ((currLevel[i][j]==12)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          g=1;
          hg=0;
        }
        if ((currLevel[i][j]==12)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          g=1;
          hg=0;
        }
        //change left
        if ((currLevel[i][j]==13)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          g=0;
          hg=-1;
        }
        if ((currLevel[i][j]==13)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          g=0;
          hg=-1;
        }
        if ((currLevel[i][j]==13)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          g=0;
          hg=-1;
        }
        if ((currLevel[i][j]==13)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          g=0;
          hg=-1;
        }
        //gate
        if ((currLevel[i][j]==14)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          pos.set(pos.x, (height/currLevel.length)*i-(ball_size[currLevelIdx]/2+1));
          vel.y*=-bounce;
        }
        if ((currLevel[i][j]==14)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          pos.set(pos.x, (height/currLevel.length)*(i+1)+(ball_size[currLevelIdx]/2+1));
          vel.y*=-bounce;
        }
        if ((currLevel[i][j]==14)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          pos.set((width/currLevel[i].length)*j-(ball_size[currLevelIdx]/2+1), pos.y);
          vel.x*=-bounce;
        }
        if ((currLevel[i][j]==14)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          pos.set((width/currLevel[i].length)*(j+1)+(ball_size[currLevelIdx]/2+1), pos.y);
          vel.x*=-bounce;
        }
        //gate key
        if ((currLevel[i][j]==15)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          if (currLevel[i][j]>0) {
            currLevel[i][j]*=-1;
          }
        }
        if ((currLevel[i][j]==15)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          if (currLevel[i][j]>0) {
            currLevel[i][j]*=-1;
          }
        }
        if ((currLevel[i][j]==15)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          if (currLevel[i][j]>0) {
            currLevel[i][j]*=-1;
          }
        }
        if ((currLevel[i][j]==15)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          if (currLevel[i][j]>0) {
            currLevel[i][j]*=-1;
          }
        }
        //halt
        if ((currLevel[i][j]==16)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y+(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y+(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          pos.set(pos.x, (height/currLevel.length)*i-(ball_size[currLevelIdx]/2+1));
          bounce/=10;
          vel.y*=-bounce;
          bounce*=10;
          vel.x*=bounce;
        }
        if ((currLevel[i][j]==16)&&(pos.x>=(width/currLevel[i].length)*j)&&(pos.x<=(width/currLevel[i].length)*(j+1))&&
          (pos.y-(ball_size[currLevelIdx]/2+1)>=(height/currLevel.length)*i)&&(pos.y-(ball_size[currLevelIdx]/2+1)<=(height/currLevel.length)*(i+1))) {
          pos.set(pos.x, (height/currLevel.length)*(i+1)+(ball_size[currLevelIdx]/2+1));
          bounce/=10;
          vel.y*=-bounce;
          bounce*=10;
          vel.x*=bounce;
        }
        if ((currLevel[i][j]==16)&&(pos.x+(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x+(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          pos.set((width/currLevel[i].length)*j-(ball_size[currLevelIdx]/2+1), pos.y);
          bounce/=10;
          vel.x*=-bounce;
          bounce*=10;
          vel.y*=bounce;
        }
        if ((currLevel[i][j]==16)&&(pos.x-(ball_size[currLevelIdx]/2+1)>=(width/currLevel[i].length)*j)&&(pos.x-(ball_size[currLevelIdx]/2+1)<=(width/currLevel[i].length)*(j+1))&&
          (pos.y>=(height/currLevel.length)*i)&&(pos.y<=(height/currLevel.length)*(i+1))) {
          pos.set((width/currLevel[i].length)*(j+1)+(ball_size[currLevelIdx]/2+1), pos.y);
          bounce/=10;
          vel.x*=-bounce;
          bounce*=10;
          vel.y*=bounce;
        }
      }
    }
  }

  void show() {
    fill(0, 255, 0);
    noStroke();
    ellipse(pos.x, pos.y, ball_size[currLevelIdx], ball_size[currLevelIdx]);
    for (int i=0; i<currLevel.length; i++) {
      for (int j=0; j<currLevel[i].length; j++) {
        noFill();
        noStroke();
        if (currLevel[i][j]==2) {
          fill(255);
          stroke(255);
        } else if (currLevel[i][j]==3) {
          fill(0, 255, 0, 100);
          stroke(0, 255, 0, 100);
        } else if (currLevel[i][j]==4) {
          fill(255, 0, 0);
          stroke(255, 0, 0);
        } else if (currLevel[i][j]==5) {
          fill(255, 255, 0, 100);
          stroke(255, 255, 0, 100);
        } else if (currLevel[i][j]==6) {
          fill(0, 0, 255, 100);
          stroke(0, 0, 255, 100);
        } else if (currLevel[i][j]==7) {
          fill(0, 255, 255, 100);
          stroke(0, 255, 255, 100);
        } else if (currLevel[i][j]==8) {
          fill(180, 100, 100);
          stroke(180, 100, 100);
        } else if (currLevel[i][j]==9) {
          fill(180, 100, 50, 100);
          stroke(180, 100, 50, 100);
        } else if (currLevel[i][j]==14) {
          fill(100, 25, 25);
          stroke(100, 25, 25);
        } else if (currLevel[i][j]==15) {
          fill(255, 225, 125);
          stroke(255, 225, 125);
        } else if (currLevel[i][j]==16) {
          fill(0);
          stroke(0);
        } else {
          noFill();
          noStroke();
        }
        rect((width/currLevel[i].length)*j, (height/currLevel.length)*i, (width/currLevel[i].length), (height/currLevel.length));
        if (currLevel[i][j]==10) {
          up((width/currLevel[i].length)*j, (height/currLevel.length)*i, (width/currLevel[i].length), (height/currLevel.length));
        }
        if (currLevel[i][j]==11) {
          right((width/currLevel[i].length)*j, (height/currLevel.length)*i, (width/currLevel[i].length), (height/currLevel.length));
        }
        if (currLevel[i][j]==12) {
          down((width/currLevel[i].length)*j, (height/currLevel.length)*i, (width/currLevel[i].length), (height/currLevel.length));
        }
        if (currLevel[i][j]==13) {
          left((width/currLevel[i].length)*j, (height/currLevel.length)*i, (width/currLevel[i].length), (height/currLevel.length));
        }
      }
    }
  }
}

void left(float x, float y, float w, float h) {
  noStroke();
  strokeWeight(1.5);
  fill(255, 10, 255, 125);
  rect(x, y, w, h);
  fill(0);
  stroke(0);
  line(x, y+(h/2), x+(w/2), y+h);
  line(x+(w/2), y+(h/2), x+w, y+(h/2));
  line(x, y+(h/2), x+(w/2), y);
  line(x+(w/2), y, x+(w/2), y+h);
  noStroke();
}
void up(float x, float y, float w, float h) {
  noStroke();
  strokeWeight(1.5);
  fill(255, 10, 255, 125);
  rect(x, y, w, h);
  fill(0);
  stroke(0);
  line(x, y+(h/2), x+w, y+(h/2));
  line(x+(w/2), y, x, y+(h/2));
  line(x+(w/2), y, x+w, y+(h/2));
  line(x+(w/2), y+(w/2), x+(w/2), y+h);
  noStroke();
}
void down(float x, float y, float w, float h) {
  noStroke();
  strokeWeight(1.5);
  fill(255, 10, 255, 125);
  rect(x, y, w, h);
  fill(0);
  stroke(0);
  line(x, y+(h/2), x+w, y+(h/2));
  line(x+(w/2), y+h, x, y+(h/2));
  line(x+(w/2), y+h, x+w, y+(h/2));
  line(x+(w/2), y+(w/2), x+(w/2), y);
  noStroke();
}
void right(float x, float y, float w, float h) {
  noStroke();
  strokeWeight(1.5);
  fill(255, 10, 255, 125);
  rect(x, y, w, h);
  fill(0);
  stroke(0);
  line(x+w, y+(h/2), x+(w/2), y+h);
  line(x+w, y+(h/2), x+(w/2), y);
  line(x+(w/2), y, x+(w/2), y+h);
  line(x+(w/2), y+(w/2), x, y+(h/2));
  noStroke();
}
