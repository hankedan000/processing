List<Bloon> inRange(List<Bloon> all, PVector s, float radius)
{
  List<Bloon> l = new ArrayList();
  
  Iterator<Bloon> itr = all.iterator();
  PVector dist = new PVector();
  while (itr.hasNext())
  {
    Bloon b = itr.next();
    dist.set(b.s);
    dist.sub(s);
    if (dist.mag() < radius)
    {
      l.add(b);
    }
  }
  
  return l;
}

Bloon furthest(List<Bloon> all)
{
  Bloon furthestBloon = null;
  
  Iterator<Bloon> itr = all.iterator();
  while (itr.hasNext())
  {
    Bloon b = itr.next();
    if (furthestBloon == null ||
        b.distance > furthestBloon.distance)
    {
      furthestBloon = b;
    }
  }
  
  return furthestBloon;
}

Bloon strongest(List<Bloon> all)
{
  Bloon strongestBloon = null;
  
  Iterator<Bloon> itr = all.iterator();
  while (itr.hasNext())
  {
    Bloon b = itr.next();
    if (strongestBloon == null ||
        b.health > strongestBloon.health)
    {
      strongestBloon = b;
    }
  }
  
  return strongestBloon;
}
