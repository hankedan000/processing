class Button
{
  color bg;
  color fg;
  boolean visible;
  int x,y,w,h;
  int border;
  String text;
  Tower tower;
  
  public Button(int x, int y, int w, int h)
  {
    this("",x,y,w,h);
  }
  
  public Button(Tower t, int x, int y, int w, int h)
  {
    this("",x,y,w,h);
    tower = t;
    tower.setVisible(true);
    tower.s.x = x+w/2;
    tower.s.y = y+h/2;
  }
  
  public Button(String str, int x, int y, int w, int h)
  {
    bg = #aaaaaa;
    fg = #000000;
    visible = true;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    border = 2;
    text = str;
    tower = null;
  }
  
  public void setBackground(color c)
  {
    bg = c;
  }
  
  public void setText(String str)
  {
    text = str;
  }
  
  public void setVisible(boolean visible)
  {
    this.visible = visible;
  }
  
  public boolean isHovering()
  {
    boolean hover = false;
    if (mouseX > x && mouseX < x+w &&
        mouseY > y && mouseY < y+h)
    {
      hover = true;
    }
    return hover;
  }
  
  public void draw()
  {
    if (visible)
    {
      fill(bg); stroke(fg); strokeWeight(border);
      rect(x,y,w-border,h-border);
      
      if (tower != null && tower.isVisible())
      {
        tower.draw();
      }
      
      if (isHovering())
      {
        // to soften the background color while hovering
        fill(255,50);
        rect(x,y,w-border,h-border);
      }
      
      if (!text.isEmpty())
      {
        fill(fg);
        textAlign(CENTER,CENTER);
        text(text,x+w/2+border,y+h*1/3+border);
      }
    }
  }
}
