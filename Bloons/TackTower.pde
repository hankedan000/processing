class TackTower extends Tower
{
  int nTacks = 10;
  ArrayList<PVector> tackLocs = new ArrayList();
  ArrayList<Dart> myDarts = new ArrayList();
  
  public TackTower(Map map)
  {
    super(map);
    
    onTrackOnly = true;
    setRange(0.5);
    for (int n=0; n<nTacks; n++)
    {
      float randX = random(tileW/2)-tileW/4;
      float randY = random(tileW/2)-tileW/4;
      tackLocs.add(new PVector(randX,randY));
    }
  }
  
  @Override
  public void setActive(boolean isActive)
  {
    super.setActive(isActive);
    
    for (int i=0; i<tackLocs.size(); i++)
    {
      Dart d = new Dart(tackLocs.get(i), new PVector());
      d.s.add(s);
      d.v.set(0,0);// no velocity
      myDarts.add(d);
      spawn(d);
    }
  }
   
  @Override
  protected void spawnDarts(Bloon target)
  {
    // doesn't shoot darts...
  }
  
  @Override
  protected void drawBody()
  {
    if (!active)
    {
      for (int i=0; i<tackLocs.size(); i++)
      {
        Dart d = new Dart(tackLocs.get(i), new PVector());
        d.init(map);
        d.s.add(s);
        d.v.set(0,0);// no velocity
        d.draw();
      }
    }
    else
    {
      Iterator<Dart> dItr = myDarts.iterator();
      while (dItr.hasNext())
      {
        Dart d = dItr.next();
        if (!d.isAlive())
        {
          dItr.remove();
        }
      }
    }
    
    alive = myDarts.size() > 0;
  }
}
