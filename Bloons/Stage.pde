class Stage
{
  class Group
  {
    int delay;
    int intvl;
    int num;
    char type;
    
    public Group(JSONObject jo)
    {
      delay = jo.getInt("d",0);
      intvl = jo.getInt("i",0);
      num   = jo.getInt("n",0);
      String typeStr = jo.getString("t","r");
      if (typeStr.length() >= 1)
      {
        type = typeStr.charAt(0);
      }
      else
      {
        println("Group - Not enough chars in type string!");
      }
    }
    
    public Group()
    {
      delay = 0;
      intvl = 0;
      num = 0;
      type = 'r';
    }
  }
  Queue<Group> groups = new LinkedList();
  
  public Stage(JSONArray ja)
  {
    for (int jj=0; jj<ja.size(); jj++)
    {
      groups.add(new Group(ja.getJSONObject(jj)));
    }
  }
  
  public Stage(String str)
  {
    Scanner sc = new Scanner(str);
    sc.useDelimiter(",");
    
    Group g = new Group();
    while (sc.hasNext())
    {
      if (sc.hasNextInt())
      {
        g.delay = sc.nextInt();
      }
      else if (sc.hasNext())
      {
        String word = sc.next();
        if (word.length() == 1)
        {
          g.type = word.charAt(0);
          groups.add(g);
          g = new Group();
        }
        else
        {
          println("Invalid type '" + word + "'");
        }
      }
    }
    
    sc.close();
  }
}
