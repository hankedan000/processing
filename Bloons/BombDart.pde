class BombDart extends Dart
{
  public BombDart(PVector initS, PVector heading)
  {
    super(initS,heading);
    setTravelDistance(3);// 3 tiles initially
    ttl = 5;
    // radius = RADIUS_SCALE * tileW
    RADIUS_SCALE = 0.2;
    MAX_TPS = 4;
  }
}
