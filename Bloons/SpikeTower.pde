class SpikeTower extends Tower
{
  float cannonH;
  float cannonW;
  // number of double ended cannons
  int nCannons;
  
  public SpikeTower(Map map)
  {
    super(map);
    cannonW = w*1.2;
    cannonH = w*0.5;
    nCannons = 4;
    setRange(1.5);
  }
  
  @Override
  protected void spawnDarts(Bloon target)
  {
    // start with down heading dart
    PVector h = new PVector(0,1);
    
    int nDarts = nCannons*2;
    float aStep = TWO_PI/nDarts;
    for (int n=0; n<nDarts; n++)
    {
      Dart d = new Dart(s,h);
      d.setTravelDistance(range_tiles);
      spawn(d);
      h.rotate(aStep);
    }
  }
  
  @Override
  protected void drawBody()
  {
    fill(#E85AEA); noStroke();
    ellipse(s.x,s.y,w,w);
  }
}
