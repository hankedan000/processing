class Map
{
  int x,y;
  char[][] terrain;
  int startX,startY;
  int finishX,finishY;
  Path simplePath;
  Path path;
  int stepsPerTile = 1;
  
  public Map(int x, int y, String terrain)
  {
    if (terrain.length() <= x*y)
    {
      this.x = x;
      this.y = y;
      this.terrain = new char[x][y];
      for (int i=0; i<terrain.length(); i++)
      {
        this.terrain[i%x][i/x] = terrain.charAt(i);
        
        switch (terrain.charAt(i))
        {
          case 's':
            startX = i%x;
            startY = i/x;
            break;
          case 'f':
            finishX = i%x;
            finishY = i/x;
            break;
        }
      }
    }
    else
    {
      println("Invalid terrain!");
    }
    
    simplePath = new Path();
    path = new Path();
    plotPath();
    subDividePath();
    subDividePath();
    subDividePath();
    subDividePath();
    subDividePath();
  }
  
  public int getTileW()
  {
    return width / x;
  }
  
  private PVector findNextHeading(int x, int y, PVector cameFrom)
  {
    PVector h = new PVector();
    
    if (x < (this.x-1) &&
        (terrain[x+1][y] == 'r' || terrain[x+1][y] == 'f') &&
        cameFrom.x != -1)
    {
      // right;
      //println("("+x+","+y+") right");
      h.set(1,0);
    }
    else if (y < (this.y-1) &&
             (terrain[x][y+1] == 'r' || terrain[x][y+1] == 'f') &&
             cameFrom.y != -1)
    {
      // down
      //println("("+x+","+y+") down");
      h.set(0,1);
    }
    else if (x > 0 &&
             (terrain[x-1][y] == 'r' || terrain[x-1][y] == 'f') &&
             cameFrom.x != 1)
    {
      // left
      //println("("+x+","+y+") left");
      h.set(-1,0);
    }
    else if (y > 0 &&
             (terrain[x][y-1] == 'r' || terrain[x][y-1] == 'f') &&
             cameFrom.y != 1)
    {
      // up
      //println("("+x+","+y+") up");
      h.set(0,-1);
    }
    
    return h;
  }
  
  void plotPath()
  {
    int x = startX;
    int y = startY;
    int n = 0;
    
    PVector h = new PVector();
    while (terrain[x][y] != 'f' && n<(this.x*this.y))
    {
      //println("adding x=" + x + ", y=" + y);
      path.add(new PVector(x,y));
      simplePath.add(new PVector(x,y));
      h = findNextHeading(x,y,h);
      x += h.x;
      y += h.y;
      n++;
    }
    
    if (terrain[x][y] == 'f')
    {
      path.add(new PVector(x,y));
    }
  }
  
  float getStepsPerSec(float tilesPerSec)
  {
    return tilesPerSec * stepsPerTile;
  }
  
  void subDividePath()
  {
    path.subDivide();
    stepsPerTile *= 2;
  }
  
  boolean isTouchingPath(Tower t)
  {
    boolean isTouching = false;
    float tileW = width/x;
    
    Iterator<PVector> itr = simplePath.nodes.iterator();
    PVector d = new PVector();
    while (itr.hasNext())
    {
      PVector p = itr.next();
      d.set(t.s);
      d.sub(p.x*tileW+tileW/2,p.y*tileW+tileW/2);
      if (d.mag() < (t.w/2 + tileW/2))
      {
        isTouching = true;
        break;
      }
    }
    
    return isTouching;
  }
}
