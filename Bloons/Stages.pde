ArrayList<Stage> stages = new ArrayList();

void initStages()
{
  JSONArray jsStages = loadJSONArray("stages.json");
  for (int jj=0; jj<jsStages.size(); jj++)
  {
    stages.add(new Stage(jsStages.getJSONArray(jj)));
  }
}
