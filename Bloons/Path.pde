import java.util.*;

class Path
{
  LinkedList<PVector> nodes;
  
  public Path()
  {
    nodes = new LinkedList();
  }
  
  public void add(PVector p)
  {
    nodes.add(p);
  }
  
  public long size()
  {
    return nodes.size();
  }
  
  public PVector get(int i)
  {
    return nodes.get(i);
  }
  
  public void subDivide()
  {
    if (nodes.size() < 1)
    {
      return;
    }
    ListIterator<PVector> itr = nodes.listIterator();
    PVector n1 = new PVector();
    n1.set(itr.next());
    while (itr.hasNext() && nodes.size() > 2)
    {
      PVector n2 = new PVector();
      n2.set(itr.next());
      // back pedal and insert between n1 and n2
      itr.previous();
      n2.sub(n1).div(2).add(n1);
      itr.add(n2);
      n1.set(itr.next());
    }
  }
  
  ListIterator<PVector> getPathIterator()
  {
    return nodes.listIterator();
  }
  
  void print()
  {
    for (int i=0; i<nodes.size(); i++)
    {
      PVector p = nodes.get(i);
      println(i + ": x=" + p.x + "; y=" + p.y);
    }
  }
}
