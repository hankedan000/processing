class Tower
{
  PVector s;
  // radius around tower in units of tile width
  float range_tiles;
  float range_px;
  float tileW;
  float w;
  int damage;
  // darts per second
  float dps;
  long lastFireTime;
  boolean canFire;
  Bloon target;
  color rangeColor = #00ff00;
  float heading = 0;
  
  boolean alive;
  boolean drawRange;
  boolean visible;
  boolean active;
  boolean trackFurthest;
  // true if tower can only be on track
  boolean onTrackOnly;
  
  public Tower(Map map)
  {
    s = new PVector();
    tileW = map.getTileW();
    w = tileW*2/3;
    setRange(2);
    damage = 1;
    dps = 1.0/2.0;
    canFire = true;
    alive = true;
    drawRange = false;
    visible = true;
    active = false;
    trackFurthest = true;
    onTrackOnly = false;
  }
  
  protected void setRange(float nTiles)
  {
    range_tiles = nTiles;
    range_px = nTiles * tileW;
  }
  
  public void setActive(boolean isActive)
  {
    active = isActive;
  }
  
  public void setVisible(boolean isVisible)
  {
    visible = isVisible;
  }
  
  public boolean isVisible()
  {
    return visible;
  }
  
  public void setShowRange(boolean show)
  {
    drawRange = show;
  }
  
  public void setRangeColor(color c)
  {
    rangeColor = c;
  }
  
  public void setFurthestOrNearest(boolean furthest)
  {
    trackFurthest = furthest;
  }
  
  public void setPosition(int x, int y)
  {
    s.x = x;
    s.y = y;
  }
  
  public boolean isTouching(Tower other)
  {
    boolean isTouching = false;
    
    PVector d = new PVector();
    d.set(other.s);
    d.sub(s);
    if (d.mag() < (w/2 + other.w/2))
    {
      isTouching = true;
    }
    
    return isTouching;
  }
  
  public void track(List<Bloon> bloons)
  {
    List<Bloon> inRange = inRange(bloons,s,range_px);
    
    // draw lines for all bloons in my range
    //Iterator<Bloon> itr = inRange.iterator();
    //while (itr.hasNext())
    //{
    //  Bloon b = itr.next();
    //  fill(0); strokeWeight(2); stroke(0);
    //  line(b.s.x,b.s.y,s.x,s.y);
    //}
    
    target = furthest(inRange);
    if (target != null && target.isAlive())
    {
        fill(0); strokeWeight(2); stroke(0);
        line(target.s.x,target.s.y,s.x,s.y);
    }
    
    if (active)
    {
      long time = millis();
      float dt_ms = 1000/dps;
      if ((time - lastFireTime)*gameSpeed > dt_ms)
      {
        canFire = true;
      }
      
      if (target != null)
      {
        PVector dir = new PVector();
        dir.set(target.s);
        dir.sub(s);
        heading = dir.heading() + PI / 2;
      }
      
      if (canFire && target != null && target.isAlive())
      {
        fill(0); strokeWeight(2); stroke(#00ff80);
        line(target.s.x,target.s.y,s.x,s.y);
        shoot(target);
      }
    }
  }
  
  protected void spawnDarts(Bloon target)
  {
    PVector h = new PVector();
    h.set(target.s).sub(s);
    spawn(new Dart(s,h));
  }
  
  final public void shoot(Bloon target)
  {
    canFire = false;
    lastFireTime = millis();
    
    spawnDarts(target);
  }
  
  protected void drawBody()
  {
    int nW = (int)(w);
    int nH = dartMonkeyImg.height*nW/dartMonkeyImg.width;
    pushMatrix();
    translate(s.x,s.y);
    rotate(heading);
    translate(-nW/2,-nH/2);
    image(dartMonkeyImg,0,0,nW,nH);
    popMatrix();
    //fill(#816005); noStroke();
    //ellipse(s.x,s.y,w,w);
  }
  
  final public void draw()
  {
    if (visible)
    {
      if (drawRange)
      {
        fill(rangeColor,50); noStroke();
        ellipse(s.x,s.y,range_px*2,range_px*2);
      }
      drawBody();
    }
  }
}
