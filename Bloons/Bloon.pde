class Bloon
{
  int health;
  float spt = 0;
  // velocity (tiles per second)
  float v_tps = 0;
  Path path;
  float distance = 0;
  PVector s = new PVector();
  int tileW;
  color fillColor;
  boolean active;
  char c;
  int worth = 10;
  float radius = 0; 
  
  public Bloon(char type)
  {
    active = false;
    setType(type);
    switch (type)
    {
      case 'r':
        health = 1;
        break;
      case 'b':
        health = 1;
        break;
      case 'g':
        health = 1;
        break;
      case 'y':
        health = 1;
        break;
      case 'B':
        health = 1;
        break;
      case 'w':
        health = 1;
        break;
    }
  }
  
  public void setType(char type)
  {
    this.c = type;
    switch (type)
    {
      case 'r':
        v_tps = 3.0;
        fillColor = #ff0000;
        break;
      case 'b':
        v_tps = 4.0;
        fillColor = #0000ff;
        break;
      case 'g':
        v_tps = 5.0;
        fillColor = #0DBF4D;
        break;
      case 'y':
        v_tps = 10.0;
        fillColor = #EAFA14;
        break;
      case 'p':
        v_tps = 11.0;
        fillColor = #F5A6E6;
        break;
      case 'B':
        v_tps = 5.0;
        fillColor = #000000;
        break;
      case 'w':
        v_tps = 6.0;
        fillColor = #ffffff;
        break;
    }
  }
  
  public ArrayList<Character> getChildren()
  {
    ArrayList<Character> children = new ArrayList();
    
    switch (c)
    {
      case 'r':
        // none
        break;
      case 'b':
        children.add('r');
        break;
      case 'g':
        children.add('b');
        break;
      case 'y':
        children.add('g');
        break;
      case 'p':
        children.add('y');
        break;
      case 'B':
        children.add('p');
        children.add('p');
        break;
      case 'w':
        children.add('p');
        children.add('p');
        break;
    }
    
    return children;
  }
  
  public boolean isAlive()
  {
    return health > 0;
  }
  
  public void pop(int count)
  {
    money += count * worth;
    health -= count;
  }
  
  public boolean active()
  {
    return active;
  }
  
  public void setActive(boolean isActive)
  {
    active = isActive;
  }
  
  public void spawn(Map m)
  {
    spt = map.getStepsPerSec(1);
    path = m.path;
    tileW = map.getTileW();
    active = true;
    radius = map.getTileW()*2/3/2;
  }
  
  public void move(float dt)
  {
    if (active)
    {
      // compute delta step for given delta seconds
      float sps = spt * v_tps;
      float ds = sps * dt;
      distance += ds;
      int pIdx = floor(distance);
      if (pIdx < path.size())
      {
        PVector p = path.get(pIdx);
        //println(c + " distance = " + distance + "; pIdx = " + pIdx + "; p.x = " + p.x + "; p.y = " + p.y);
        s.set(p.x*tileW+tileW/2,p.y*tileW+tileW/2);
      }
      else
      {
        // TODO handle bloon escape logic
        health = 0;
      }
    }
  }
  
  public void draw()
  {
    if (active && isAlive())
    {
      PImage img = null;
      switch (c)
      {
        case 'r':
          img = rImg;
          break;
        case 'b':
          img = bImg;
          break;
        case 'g':
          img = gImg;
          break;
        case 'y':
          img = yImg;
          break;
        case 'p':
          img = pImg;
          break;
        case 'B':
          img = BImg;
          break;
        case 'w':
          img = wImg;
          break;
        default:
          // no image
          break;
      }
      
      if (img != null)
      {
        int nW = (int)(radius*2);
        int nH = img.height*nW/img.width;
        pushMatrix();
        translate(-nW/2,-nH/2);
        image(img,s.x,s.y,nW,nH);
        popMatrix();
      }
      else
      {
        fill(fillColor);
        ellipse(s.x,s.y,radius*2,radius*2);
      }
    }
  }
}
