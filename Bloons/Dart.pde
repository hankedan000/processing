class Dart
{
  PVector s = new PVector();
  PVector v = new PVector();
  float dist;
  float distToLive_tiles;
  float distToLive_px;
  float r;
  float RADIUS_SCALE = 2.5/27.0;
  // max tiles per second
  float MAX_TPS = 10;
  // max pixels per second
  float maxPPS;
  float tileW; 
  // time to live (number of pops it can make)
  int ttl;
  
  public Dart(PVector initS, PVector heading)
  {
    s.set(initS);
    v.set(heading.normalize());
    setTravelDistance(50);
    ttl = 1;
  }
  
  public boolean isAlive()
  {
    return ttl > 0;
  }
  
  public float radius()
  {
    return r;
  }
  
  public void setTravelDistance(float nTiles)
  {
    distToLive_tiles = nTiles;
    distToLive_px = distToLive_tiles * tileW;
  }
  
  public void init(Map map)
  {
    tileW = map.getTileW();
    r = tileW * RADIUS_SCALE;
    maxPPS = MAX_TPS * tileW;
    v = v.normalize();
    v.mult(maxPPS);
    setTravelDistance(distToLive_tiles);
  }
  
  public void checkCollision(List<Bloon> bloons)
  {
    Iterator<Bloon> bItr = bloons.iterator();
    PVector d = new PVector(); //<>//
    while (bItr.hasNext())
    {
      Bloon b = bItr.next();
      d.set(s);
      d.sub(b.s);
      if (b.isAlive() && d.mag() < (b.radius + r))
      {
        ttl -= 1;
        b.pop(1);
        break;
      }
    }
  }
  
  public void move(float dt)
  {
    PVector ds = new PVector();
    ds.set(v).mult(dt);
    s.add(ds); //<>//
    dist += ds.mag();
    boolean alive = true;
    alive = alive && dist < distToLive_px;
    alive = alive && s.x > 0;
    alive = alive && s.x < width;
    alive = alive && s.y > 0;
    alive = alive && s.y < width;
    if (!alive)
    {
      ttl = 0;
    }
  }
  
  public void draw()
  {
    fill(0); noStroke();
    ellipse(s.x,s.y,r*2,r*2);
  }
}
