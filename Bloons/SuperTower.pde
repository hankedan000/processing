class SuperTower extends Tower
{  
  public SuperTower(Map map)
  {
    super(map);
    dps = 10;
    setRange(5);
  }
  
  @Override
  protected void drawBody()
  {
    fill(#4964E8); noStroke();
    ellipse(s.x,s.y,w,w);
  }
}
