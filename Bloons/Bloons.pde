import java.util.*;

Map map = new Map(11,11,
                "dddddddddsd"+
                "drrrrrrrdrd"+
                "drdddddrrrd"+
                "drdrrrddddd"+
                "drdrdrdrrrd"+
                "drrrdrrrdrd"+
                "dddddddddrd"+
                "frrrrrrddrd"+
                "wwwwddrddrd"+
                "wwwwddrrrrd"+
                "wwwwddddddd");
ArrayList<Bloon> bloons = new ArrayList();
int stageIdx = -1;
Stage cStage;
long prevSpawnTime = 0;
long nextSpawnDelay = 0;
ArrayList<Tower> towers = new ArrayList();
Tower nextTower;
boolean validTowerLocation = true;
boolean mouseOnMap = false;
boolean play = false;
float gameSpeed = 1.0;
Button playButton;
Button dartTowerButton;
Button spikeTowerButton;
Button superTowerButton;
Button tackTowerButton;
int money = 200;
ArrayList<Dart> darts = new ArrayList();

PImage rImg,bImg,gImg,yImg,pImg,BImg,wImg;
PImage dartMonkeyImg;

void setup()
{
  size(600,800);
  setupControlPanel();
  
  rImg = loadImage("red.png");
  bImg = loadImage("blue.png");
  gImg = loadImage("green.png");
  yImg = loadImage("yellow.png");
  pImg = loadImage("pink.png");
  BImg = loadImage("black.png");
  dartMonkeyImg = loadImage("dart_monkey.png");
  
  wImg = loadImage("white.png");
  
  nextTower = new Tower(map);
  nextTower.setShowRange(true);
  //nextTower.setPosition(width*10/11,width*2/11);
  //nextTower.setActive(true);
  //spawnTower();
  initStages();
  loadNextStage();
}

void setupControlPanel()
{
  int panelH = height - width;
  int playW = width / 4;
  int playH = panelH / 4;
  
  int towersX = 5;
  int towersY = 3;
  int towerButtonW = width*2/3/towersX;
  
  playButton = new Button("Play",width-playW,height-playH,playW,playH);
  playButton.setBackground(#00ff00);
  playButton.setVisible(true);
  
  dartTowerButton = new Button(new Tower(map),0*towerButtonW,width+0*towerButtonW,towerButtonW,towerButtonW);
  dartTowerButton.setVisible(true);
  
  spikeTowerButton = new Button(new SpikeTower(map),1*towerButtonW,width+0*towerButtonW,towerButtonW,towerButtonW);
  spikeTowerButton.setVisible(true);
  
  tackTowerButton = new Button(new TackTower(map),2*towerButtonW,width+0*towerButtonW,towerButtonW,towerButtonW);
  tackTowerButton.setVisible(true);
  
  superTowerButton = new Button(new SuperTower(map),0*towerButtonW,width+1*towerButtonW,towerButtonW,towerButtonW);
  superTowerButton.setVisible(true);
}

void spawn(Dart d)
{
  d.init(map);
  darts.add(d);
}

void loadNextStage()
{
  bloons.clear();
  gameSpeed = 1.0;
  stageIdx++;
  
  if (stageIdx < stages.size())
  {
    cStage = stages.get(stageIdx);
  }
  
  if (cStage != null && cStage.groups.size() > 0)
  {
    nextSpawnDelay = cStage.groups.element().delay;
  }
}

void show(Map m)
{
  int w = m.getTileW();
  
  for (int x=0; x<m.x; x++)
  {
    for (int y=0; y<m.y; y++)
    {
      noStroke();
      switch (m.terrain[x][y])
      {
        case 'r':
        case 's':
        case 'f':
          fill(100);
          break;
        case 'd':
          fill(#B98C47);
          break;
        case 'w':
          fill(#62A4C6);
          break;
      }
      rect(x*w,y*w,w,w);
    }
  }
  
  //ListIterator<PVector> itr = map.path.getPathIterator();
  //while (itr.hasNext())
  //{
  //  PVector p = itr.next();
  //  fill(0);
  //  ellipse(p.x*w+w/2,p.y*h+h/2,2,2);
  //}
}

void showBloons(float dt)
{
  for (int i=0; i<bloons.size(); i++)
  {
    Bloon b = bloons.get(i);
    b.move(dt);
    b.draw();
  }
}

void mouseClicked()
{
  if (mouseOnMap)
  {
    spawnTower();
  }
  else
  {
    if (playButton.isHovering())
    {
      if (play)
      {
        // when playing the game this button controls
        // play speed
        if (gameSpeed > 1)
        {
          gameSpeed = 1;
          playButton.setText("x4");
        }
        else
        {
          gameSpeed = 4;
          playButton.setText("x1");
        }
      }
      else
      {
        playButton.setText("x4");
        playButton.setBackground(#0000ff);
        play = true;
      }
    }
    else if (dartTowerButton.isHovering())
    {
      nextTower = new Tower(map);
    }
    else if (spikeTowerButton.isHovering())
    {
      nextTower = new SpikeTower(map);
    }
    else if (tackTowerButton.isHovering())
    {
      nextTower = new TackTower(map);
    }
    else if (superTowerButton.isHovering())
    {
      nextTower = new SuperTower(map);
    }
    
    nextTower.setShowRange(true);
  }
}

void spawnTower()
{
  if (validTowerLocation && money >= 200)
  {
    nextTower.setActive(true);
    nextTower.setShowRange(false);
    towers.add(nextTower);
    nextTower = new Tower(map);
    nextTower.setShowRange(true);
    money -= 200;
  }
}

void drawControlPanel()
{
  fill(#555555); noStroke();
  rect(0,width,width,height-width);
  
  playButton.draw();
  dartTowerButton.draw();
  spikeTowerButton.draw();
  tackTowerButton.draw();
  superTowerButton.draw();
  
  fill(0);
  textAlign(LEFT,BOTTOM);
  text("Level: "+(stageIdx+1),width-100,width+10*1);
  text("Money: "+money,width-100,width+10*2);
  text("FPS: "+(int)frameRate,width-100,width+10*3);
}

void drawDarts(float dt)
{
  Iterator<Dart> dItr = darts.iterator();
  while (dItr.hasNext())
  {
    Dart d = dItr.next();
    d.move(dt);
    d.checkCollision(bloons);
    d.draw();
    if (!d.isAlive())
    {
      dItr.remove();
    }
  }
}

void draw()
{
  mouseOnMap = mouseY < width;
  background(#555555);
  nextTower.s.x = mouseX;
  nextTower.s.y = mouseY;
  // only draw placeable tower when mouse is above map
  nextTower.setVisible(mouseOnMap);
  validTowerLocation = mouseOnMap;
  if (nextTower.onTrackOnly)
  {
    validTowerLocation = validTowerLocation && map.isTouchingPath(nextTower);
  }
  else
  {
    validTowerLocation = validTowerLocation && !map.isTouchingPath(nextTower);
  }
  
  Iterator<Tower> tItr = towers.iterator();
  while (validTowerLocation && tItr.hasNext())
  {
    validTowerLocation = !nextTower.isTouching(tItr.next());
  }
  nextTower.setRangeColor(#00ff00);
  if (!validTowerLocation)
  {
    nextTower.setRangeColor(#ff0000);
  }
  
  float dt = 1.0/frameRate*gameSpeed;
  show(map);
  drawDarts(dt);
  showBloons(dt);
  
  if (cStage != null && !cStage.groups.isEmpty() && play)
  {
    long time = millis();
    //println("framerate = " + frameRate);
    //println("time = " + time);
    if ((time - prevSpawnTime) > nextSpawnDelay/gameSpeed)
    {
      //println("spawning");
      Stage.Group g = cStage.groups.element();
      g.num--;
      Bloon b = new Bloon(g.type);
      b.spawn(map);
      bloons.add(b);
      prevSpawnTime = time;
      
      if (g.num == 0)
      {
        // if group is complete, move to next group
        cStage.groups.remove();
      }
      
      if (g.num <= 0 && !cStage.groups.isEmpty())
      {
        nextSpawnDelay = cStage.groups.element().delay;
      }
      else
      {
        nextSpawnDelay = g.intvl;
      }
    }
  }
  
  Iterator<Tower> itr = towers.iterator();
  while (itr.hasNext())
  {
    Tower t = itr.next();
    t.track(bloons);
    t.draw();
    if (!t.alive)
    {
      itr.remove();
    }
  }
  nextTower.draw();
  
  ArrayList<Bloon> newBloons = new ArrayList();
  Iterator<Bloon> bItr = bloons.iterator();
  while (bItr.hasNext())
  {
    Bloon b = bItr.next();
    if (!b.isAlive())
    {
      ArrayList<Character> children = b.getChildren();
      for (int cc=0; cc<children.size(); cc++)
      {
        Bloon child = new Bloon(children.get(cc));
        child.spawn(map);
        child.distance = b.distance;
        newBloons.add(child);
      }
      bItr.remove();
    }
  }
  bloons.addAll(newBloons);
  
  if (cStage != null && cStage.groups.isEmpty() && bloons.isEmpty())
  {
    println("Stage " + stageIdx + " is complete");
    loadNextStage();
    playButton.setText("Play");
    playButton.setBackground(#00ff00);
    play = false;
  }
  //println("There are " + bloons.size() + " bloons alive");
  
  drawControlPanel();
}
