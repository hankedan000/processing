float a = PI/4;
float l = 100;

void setup()
{
  size(500,500);
}

void draw()
{
  a = map(mouseX,0,width,0,PI/2);
  l = map(mouseY,0,height,150,50);
  background(0);
  translate(width/2,height);
  branch(l);
}

void branch(float len)
{
  float c = map(len,0,l,0,255);
  stroke(c,255-c,(c+l)%255);
  line(0,0,0,-len);
  if (len > 4)
  {
    pushMatrix();
    translate(0,-len);
    rotate(a);
    branch(len*0.67);
    popMatrix();
    
    pushMatrix();
    translate(0,-len);
    rotate(-a);
    branch(len*0.67);
    popMatrix();
  }
}
