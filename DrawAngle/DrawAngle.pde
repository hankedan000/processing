color BG = color(20,20,255);
color PRI_TRACE = 255;
color SEC_TRACE = 200;

Protractor p;

void setup()
{
  p = new Protractor();
  p.setColors(PRI_TRACE,SEC_TRACE);
  
  size(400,400);
}

void drawMousePoint()
{
  noFill();stroke(PRI_TRACE);
  ellipse(mouseX,mouseY,10,10);
}

void mousePressed()
{
  p.onClick(mouseX,mouseY);
}

void draw()
{
  background(BG);
  
  drawMousePoint();
  p.draw(mouseX,mouseY);
}
