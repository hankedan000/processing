class Protractor
{
  private PVector p1,p2;
  private int state;
  private color pri;
  private color sec;
  
  public Protractor()
  {
    p1 = new PVector();
    p2 = new PVector();
    state = 0;
    pri = 150;
    sec = 200;
  }
  
  public void setColors(color pri, color sec)
  {
    this.pri = pri;
    this.sec = sec;
  }
  
  public void onClick(long x,long y)
  {
    switch(state)
    {
      case 0:
        p1.set(mouseX,mouseY);
        break;
      case 1:
        p2.set(mouseX,mouseY);
        break;
      case 2:
        break;
    }
    
    state++;
    state %= 3;
  }
  
  public void draw(long mX, long mY)
  {
    PVector mV = new PVector(mX,mY);
    
    switch(state)
    {
      case 0:
        // draw nothing
        break;
      case 1:
        drawP1();
        drawVertex(mV);
        break;
      case 2:
        drawP1();
        drawP2();
        drawVertex(p2);
        drawVertex(mV);
        float a1 = getAbsAngle(p2);
        float a2 = getAbsAngle(mV);
        println("angle = ",degrees(getAngle(mX,mY)));
        drawArc(a1,a2);
        drawAngle(mX,mY);
        break;
    }
  }
  
  public float getAngle(long mX, long mY)
  {
    if (state != 2)
    {
      return 0;
    }
    
    PVector mV = new PVector(mX,mY);
    float a1 = getAbsAngle(p2);
    float a2 = getAbsAngle(mV);
    
    if (a2 < a1)
    {
      a2 += TWO_PI;
    }
    
    return a2 - a1;
  }
  
  /**
   * Computes an angle from (0,0) to c on a unit circle
   * centered at p1
   */
  private float getAbsAngle(PVector c)
  {
    // get 2 vectors from p1->(0,0), and p1->c
    PVector a = new PVector(1,0);
    PVector b = new PVector(c.x,c.y);
    b.sub(p1);
    
    float theta = acos(a.dot(b)/a.mag()/b.mag());
    if (b.y < 0.0)
    {
      theta = TWO_PI - theta;
    }
    
    return theta;
  }
  
  private void drawP1()
  {
    fill(255,0,0);noStroke();
    ellipse(p1.x,p1.y,2,2);
  }
  
  private void drawP2()
  {
    fill(0,255,0);noStroke();
    ellipse(p2.x,p2.y,2,2);
  }
  
  private void drawVertex(PVector b)
  {
    noFill();stroke(pri);strokeWeight(2);
    line(p1.x,p1.y,b.x,b.y);
  }

  private void drawArc(float a1, float a2)
  {
    if (a2 < a1)
    {
      a2 += TWO_PI;
    }
    noFill();stroke(pri);strokeWeight(1);
    arc(p1.x,p1.y,50,50,a1,a2);
  }
  
  private void drawAngle(long mX, long mY)
  {
    if (state == 2)
    {
      float deg = degrees(getAngle(mX,mY));
      String degStr = String.format("%.2f",deg);
      fill(sec);noStroke();
      text(degStr,p2.x+5,p2.y+5);
    }
  }
};
