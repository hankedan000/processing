boolean DEBUG = true;

ArrayList<PVector> apexTrace = new ArrayList<PVector>();
float prevAng = 0.0;

/**
 * The angle of the crank shaft
 * Units: radians
 * Range: 0.0 to TWO_PI
 */
float crankAng = 0.0;

/**
 * The diameter of the crankshaft "core"
 * Units: pixels
 * Range: 0 to (SCREEN_WIDTH / 2)
 */
long CRANK_DIA = 20;

/**
 * The diameter of the rotor lobes
 * Units pixels
 */
long LOBE_DIA = 30;

/**
 * The distance from the center of the crankshaft
 * to the center of the rotor lobes.
 * Unit: pixels
 */
long LOBE_OFFSET = 90;

long ROTOR_SIZE = 100;

/**
 * The background color
 */
color BG = color(255,255,255,1);

color PRI_TRACE = 200;
color SEC_TRACE = 200;
color PRI_FILL = 0;

void setup()
{
  size(400,400);
}

void calcCrankAngle()
{
  float wd = width;
  crankAng = mouseX / wd * TWO_PI;
  
  if (DEBUG)
  {
    println("crankAng = ",crankAng);
  }
}

void drawCrank()
{
  // width of the indicator circle
  long IND_W = CRANK_DIA / 5;
  
  pushMatrix();
  translate(width/2,height/2);
  
  // Draw crankshaft "core"
  stroke(PRI_TRACE);fill(BG);
  ellipse(0,0,CRANK_DIA,CRANK_DIA);
  
  // Draw crankshaft rotation indicator
  noStroke();fill(PRI_FILL);
  rotate(crankAng);
  ellipse(CRANK_DIA / 2,0,IND_W,IND_W);
  
  // Draw lobe 1
  stroke(PRI_TRACE);noFill();
  ellipse(LOBE_OFFSET,0,LOBE_DIA,LOBE_DIA);
  
  popMatrix();
}

void drawRotor()
{
  long xpoints[] = new long[3];
  long ypoints[] = new long[3];
  float r = ROTOR_SIZE / 2.0;
  float rAng = crankAng / 3.0;
  
  for (int i=0; i<3; i++)
  {
    float angle = i * TWO_PI / 3;
    xpoints[i] = (long)(cos(angle) * r);
    ypoints[i] = (long)(sin(angle) * r);
  }
  
  pushMatrix();
  translate(width/2,height/2);
  rotate(crankAng);
  
  // Draw rotor outline 1
  stroke(PRI_TRACE);noFill();
  ellipse(LOBE_OFFSET,0,ROTOR_SIZE,ROTOR_SIZE);
  
  translate(LOBE_OFFSET,0);
  rotate(rAng);
  triangle(xpoints[0],ypoints[0],
           xpoints[1],ypoints[1],
           xpoints[2],ypoints[2]);
  fill(255,0,0);
  for(int i=0; i<3; i++)
  {
    ellipse(xpoints[i],ypoints[i],5,5);
  }
  
  popMatrix();
}

void drawApexTrace()
{
  pushMatrix();
  translate(width/2,height/2);
  rotate(crankAng);
  for (PVector apex : apexTrace)
  {
    
  }
  popMatrix();
}

void draw()
{
  calcCrankAngle();
  
  //background(BG);
  fill(BG);
  rect(0,0,width,height);
  drawCrank();
  drawRotor();
}
