import java.util.LinkedList;
import java.util.ListIterator;

int len = 200;
int girth = 10;
int dotSize = 5;
float step = 2;
PVector dot;
LinkedList<PVector> snake = new LinkedList();

void setup()
{
  size(500,500);
  dot = new PVector();
  respawnDot();
  snake.add(new PVector(width/2,height/2));
}

void respawnDot()
{
  dot.set(random(width-girth*2)+girth,random(height-girth*2)+girth);
}

PVector getStepVector()
{
  PVector sv = new PVector(mouseX,mouseY);
  PVector head = snake.get(0);
  sv.sub(head);
  sv.setMag(step);
  return sv;
}

void move()
{
  PVector newHead = new PVector();
  newHead.set(snake.get(0));
  newHead.add(getStepVector());
  snake.addFirst(newHead);
  if (snake.size() > len)
  {
    snake.removeLast();
  }
  
  PVector dist = new PVector();
  dist.set(newHead);
  dist.sub(dot);
  if (dist.mag() < girth/2)
  {
    len++;
    respawnDot();
  }
}

void draw()
{
  background(0);
  
  fill(#ff0000); noStroke();
  ellipse(dot.x,dot.y,dotSize,dotSize);
  
  PVector s1 = snake.get(0);
  for (int i=1; i<snake.size(); i++)
  {
    PVector s2 = snake.get(i);
    stroke(255); strokeWeight(girth);
    line(s1.x,s1.y,s2.x,s2.y);
    s1 = s2;
  }
  move();
}
