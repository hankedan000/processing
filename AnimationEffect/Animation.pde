class Animation
{
  PVector si,sf,s;
  PVector v;
  PVector a;
  // total time
  float tt;
  // total animation duration
  float dur;
  
  public Animation(float xi, float yi,
                   float xf, float yf,
                   float duration)
  {
    si = new PVector(xi,yi);
    sf = new PVector(xf,yf);
    s = new PVector(xi,yi);
    v = new PVector();
    v.set(sf);
    v.sub(si);
    v.div(duration);
    a = new PVector();
    tt = 0;
    dur = duration;
  }
  
  public void restart()
  {
    s.set(si);
    v.set(sf);
    v.sub(si);
    v.div(dur);
    a.set(0,0);
    tt = 0;
  }
  
  public boolean isDone()
  {
    return tt >= dur;
  }
  
  public void sim(float dt)
  {
    PVector ds = new PVector();
    ds.set(v);
    ds.mult(dt);
    s.add(ds);
    if (tt < dur)
    {
      tt += dt;
    }
    else
    {
      s.set(sf);
      tt = dur;
    }
  }
}
