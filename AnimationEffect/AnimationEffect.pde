Animation a;
long restartFrameCount = 0;

void setup()
{
  size(400,400);
  a = new Animation(width/2,height/2+20,
                    width/2,height/2,
                    0.25);
}

void draw()
{
  background(0);
  
  if (frameCount > restartFrameCount + 20)
  {
    if (a.isDone())
    {
      a.restart();
      restartFrameCount = frameCount;
    }
    a.sim(1/frameRate);
  }
  fill(255);
  ellipse(a.s.x,a.s.y,10,10);
}
